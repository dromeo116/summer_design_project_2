"""
Copyright

Dominic V. Romeo
6/1/2019

Waveform Generator.
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy import signal as sig

print(f"Waveform Generator")

# Waveform Parameters.
waveform_duration_sec = 1.0
# Sample rate.
sample_rate_hz        = 800
# First wavefrom frequency.
frequency_hz1         = 200
# Second wavefrom frequency.
frequency_hz2         = 80
# Sampling interval.
ts= 1.0 / sample_rate_hz

# Time vector.
t=np.arange(0,waveform_duration_sec,ts)

#Uncomment the following lines to add noise to the waveform
noise = np.random.normal(0, 0.005, t.shape)
t=t+noise

#Uncomment the following lines to filter out the noise (would be part of TDC2)
#b, a = sig.butter(3, 0.05)
#t = sig.filtfilt(b, a, t)

# Generate the waveform.

#Uncomment for just one peak to classify
waveform = np.sin(2*np.pi*frequency_hz2*t)
#Uncomment for two peaks to classify
#waveform = np.sin(2*np.pi*frequency_hz1*t) + np.sin(2*np.pi*frequency_hz2*t)

plt.subplot(211)
plt.title("Time Domain Sequence")
plt.xlabel("Time Index n")
plt.ylabel("Amplitude")
plt.plot(waveform)

# Length of the signal 
n = len(waveform)
print(type(n))

# Convert waveform to float 32.
waveform_32 = waveform.astype(np.float32)
print( waveform_32[0:10] )

waveform_32_bytes = waveform_32.tobytes()
print(type(waveform_32_bytes))
print(len(waveform_32_bytes))
print(waveform_32_bytes[0:10])

# Take an fft of our waveform.
dft = np.fft.fft(waveform_32)
dft = dft[range(n//2)] # One side of the frequency spectrum. 
plt.subplot(212)
plt.title("FFT")
plt.xlabel("Frequency index k")
plt.ylabel("Magnitude")
normalized_spectrum = abs(dft) / n * 2  # Normalized.
plt.plot(normalized_spectrum) 
plt.show() 

# Determine which frequency is present. 
print( np.where( normalized_spectrum > .8 ) )

# Write wavefrom to file.
with open( f"wv{frequency_hz}hz.dat", "wb" ) as waveform_file:
    waveform_file.write(waveform_32_bytes)
    
# Open wavefile to make sure it was written correctly.
with open( f"wv{frequency_hz}hz.dat", "rb" ) as file:
    waveform_read = file.read()
    
    print(waveform_read[0:10])
    
    # Convert to float32 
    float_data = np.frombuffer(buffer=waveform_read, dtype=np.float32)
    
    print(float_data[0:10])
    
    plt.figure("Data written to file")    
    plt.plot(float_data)
    plt.show()