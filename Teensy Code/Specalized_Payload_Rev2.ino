/*
Copyright

Dominic V. Romeo 
7/7/2019

Specalized Payload
    * Writes to serial waveforms saved on SD Card. 

*/ 

#include <SPI.h>
#include <SD.h>

// Use these with the Teensy 3.5 & 3.6 SD card
#define SDCARD_CS_PIN    BUILTIN_SDCARD
#define SDCARD_MOSI_PIN  11  // not actually used
#define SDCARD_SCK_PIN   13  // not actually used

File dataFile; 
const int chipSelect = 4;
char mode = 0; 
int passive_response_delay_ms = 1000; 
int passive_ctr = 1;

void setup() {  
  // Open serial port.
  Serial.begin(115200);
  
  while (!Serial) {
    ; // wait for serial port to connect.
  }
  
  // Initialize the SD card
  SPI.setMOSI(SDCARD_MOSI_PIN);
  SPI.setSCK(SDCARD_SCK_PIN);
  if (!(SD.begin(SDCARD_CS_PIN))) {
    Serial.println("Card failed, or not present");
    
    // don't do anything more:
    while (1);
  }

  // Initalize the random seed. 
  randomSeed(analogRead(0));  
}

void loop() {
  // Determine which mode the operater wants. 
  if( Serial.available()){
    // Determine which mode we are in.
    mode = Serial.read();
    
    // Active Mode Transmit. 
    if ( mode == '1' ){      
      // Simulate a ping return by adding a delay.
      // TODO transmit this on the ping cycle 
      Serial.println("MODE DEACTIVATED. TRY ANOTHER MODE\n");
      
      // *** FUTURE Transmit waveform over the DAC. ***
    }
    // Passive Mode Listen.
    else if ( mode == '2' ){
      // Generate a response delay from 1000 to 9999 ms.
      //passive_response_delay_ms = random(1000, 10000);
      //delay(passive_response_delay_ms);
      // Send waveform for current passive mode ctr

      if(passive_ctr == 1){
        passive_ctr = passive_ctr + 1;
        send_waveform("s100.dat");
      }
      else if (passive_ctr == 2){
        passive_ctr = passive_ctr + 1;
        send_waveform("s20h.dat");
      }
      else if(passive_ctr == 3){
        passive_ctr = passive_ctr + 1;
        send_waveform("s200hn.dat");
      }
      else if(passive_ctr == 4){
        passive_ctr = passive_ctr + 1;
        send_waveform("d80160.dat");
      }
      else if(passive_ctr == 5){
        passive_ctr = 1;
        send_waveform("d80180n.dat");
      }
    }
    
    // ENV Data.
    else if( mode == '3' ){
      Serial.println("TEMPERATURE_SALINTY_PRESSURE \n");
    }
    // Contact Data. 
    else if( mode == '4' ){
      Serial.println("TARGET-BEARING-REL-DEG_OWNSHIP-HEADING-TRUE_SENSOR-DE \n");
    }
  }
}

int send_waveform(const char *file_name){
  /* Read a waveform form file and send it via serial. 
   *  
   */
  Serial.println(file_name);
  // Open the file.
  dataFile = SD.open(file_name);
  
  // If the file is available, write it to serial:
  if(dataFile){
    while (dataFile.available()) { 
      Serial.write(dataFile.read());          
    }
    // Close the file. Only one can be open at a time. 
    dataFile.close();
  }
  // If the file isn't open, send back an error.
  else{
    Serial.println("error opening waveform \n");
  }
  
  return 1; 
}
